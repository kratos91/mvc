package controllers

import (
	"net/http"

	"bitbucket.org/kratos91/mvc/domain"
	"bitbucket.org/kratos91/mvc/services"
	"bitbucket.org/kratos91/mvc/utils"
	"github.com/gin-gonic/gin"
)

type apiHandler struct {
	service services.ApiServiceImpl
}

type ApiHandlerImpl interface {
	GetTest(*gin.Context)
	PostTest(*gin.Context)
}

func (h *apiHandler) GetTest(c *gin.Context) {
	response, err := h.service.Get()
	if err != nil {
		errSer := *err
		errRes = utils.AppError{
			Message: errSer.Error(),
			Status:  http.StatusInternalServerError,
			Code:    "internal_server_error",
		}
		c.JSON(errRes.Status, errRes)
		return
	}
	c.JSON(http.StatusOK, response)
}

func (h *apiHandler) PostTest(c *gin.Context) {
	var test domain.Placeholder

	err := c.BindJSON(&test)
	if err != nil {
		errRes = utils.AppError{
			Message: err.Error(),
			Status:  http.StatusUnprocessableEntity,
			Code:    "unprocessable_entity",
		}
		c.JSON(errRes.Status, errRes)
		return
	}

	response, errSer := h.service.Post(&test)
	if errSer != nil {
		errRes = utils.AppError{
			Message: err.Error(),
			Status:  http.StatusInternalServerError,
			Code:    "internale_server_error",
		}
		c.JSON(errRes.Status, err)
		return
	}

	c.JSON(http.StatusCreated, response)
}

func NewApiHandler(service services.ApiServiceImpl) ApiHandlerImpl {
	return &apiHandler{service: service}
}
