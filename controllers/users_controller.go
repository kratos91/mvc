package controllers

import (
	"net/http"
	"strconv"

	"bitbucket.org/kratos91/mvc/services"
	"bitbucket.org/kratos91/mvc/utils"
	"github.com/gin-gonic/gin"
)

var (
	errRes utils.AppError
)

type userHandler struct {
	service services.UserServiceImpl
}

type UserHandlerImpl interface {
	GetUser(*gin.Context)
}

func (h *userHandler) GetUser(c *gin.Context) {
	userId, err := strconv.ParseInt(c.Param("user_id"), 10, 64)
	if err != nil {
		errRes = utils.AppError{
			Message: err.Error(),
			Status:  http.StatusBadRequest,
			Code:    "not_found",
		}
		utils.Respond(c, errRes.Status, err)
		return
	}

	user, errSer := h.service.GetUser(userId)
	if errSer != nil {
		errApi := *errSer
		errRes = utils.AppError{
			Message: errApi.Error(),
			Status:  http.StatusNotFound,
			Code:    "not_found",
		}
		utils.Respond(c, errRes.Status, errRes)
		return
	}
	utils.Respond(c, http.StatusOK, user)
}

func NewUserHandler(service services.UserServiceImpl) UserHandlerImpl {
	return &userHandler{service: service}
}
