package app

import (
	"bitbucket.org/kratos91/mvc/controllers"
	"bitbucket.org/kratos91/mvc/domain"
	"bitbucket.org/kratos91/mvc/services"
	"github.com/go-resty/resty"
)

func mapUrls() {
	handlerUser := controllers.NewUserHandler(services.NewUserService(domain.NewUserRepo()))
	handlerApi := controllers.NewApiHandler(services.NewApiService(domain.NewApiRepo(resty.New())))

	router.GET("/users/:user_id", handlerUser.GetUser)
	router.GET("/tests", handlerApi.GetTest)
	router.POST("/tests", handlerApi.PostTest)
}
