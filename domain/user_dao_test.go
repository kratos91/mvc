package domain

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	userRepo UserRepo
)

func TestGetUser_NoUserFound(t *testing.T) {
	//Initialization

	//Execution
	user, err := userRepo.GetUser(0)

	//Validation
	errMessage := *err
	assert.NotNil(t, err)
	assert.Nil(t, user)
	assert.EqualValues(t, fmt.Sprintf("user %d was not found", 0), errMessage.Error())
}

func TestGetUser_NoError(t *testing.T) {
	user, err := userRepo.GetUser(123)

	assert.Nil(t, err)
	assert.NotNil(t, user)
	assert.EqualValues(t, "Emiliano", user.FirstName)
}
