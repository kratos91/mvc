package domain

import (
	"encoding/json"
	"fmt"
	"github.com/go-resty/resty"
)

const (
	url = "https://jsonplaceholder.typicode.com/posts"
)

type ApiRepo interface {
	Get() (*[]Placeholder, *error)
	Post(*Placeholder) (*Placeholder, *error)
}

type apiDao struct {
	client *resty.Client
}

func (a *apiDao) Get() (*[]Placeholder, *error) {
	var placeholders []Placeholder

	r, err := a.client.R().Get(url)
	if err != nil {
		return nil, &err
	}

	errUn:=json.Unmarshal(r.Body(),&placeholders)
	if errUn != nil {
		return nil,&errUn
	}

	if !r.IsSuccess() {
		errS := fmt.Errorf("request fail with code %d", r.StatusCode())
		return nil, &errS
	}
	return &placeholders, nil
}

func (a *apiDao) Post(placeholder *Placeholder) (*Placeholder, *error) {
	var response Placeholder
	jsonBody, err := json.Marshal(placeholder)
	if err != nil {
		return nil, &err
	}
	r, errPost := a.client.R().
		SetHeader("Content-Type", "application/json").
		SetBody(string(jsonBody)).
		Post(url)

	if err != nil {
		return nil, &errPost
	}

	errUn:=json.Unmarshal(r.Body(),&placeholder)
	if errUn != nil {
		return nil,&errUn
	}

	if r.StatusCode() > 299 {
		errStatus := fmt.Errorf("error with the API,status code:%d", r.StatusCode())
		return nil, &errStatus
	}
	return &response, nil
}

func NewApiRepo(client *resty.Client) ApiRepo {
	return &apiDao{client: client}
}
