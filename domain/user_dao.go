package domain

import (
	"errors"
	"fmt"
)

var (
	users = map[int64]*User{
		123: {Id: 123, FirstName: "Emiliano", LastName: "Martinez", Email: "emiliano@gmail.com"},
	}
)

type UserRepo interface {
	GetUser(int64) (*User, *error)
}

type userDao struct{}

func (u *userDao) GetUser(userId int64) (*User, *error) {
	user := users[userId]
	if user == nil {
		err := errors.New(fmt.Sprintf("user %v was not found", userId))
		return nil, &err
	}
	return user, nil
}

func NewUserRepo() UserRepo {
	return &userDao{}
}
