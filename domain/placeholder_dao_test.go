package domain

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"

	"github.com/go-resty/resty"
	"github.com/jarcoal/httpmock"
)

const (
	jsonBody = `[
		{
			"id": 1,
			"title": "sunt aut facere repellat provident",
			"body": "quia et suscipitn suscipitn recusandae consequuntur expedita et cumreprehenderit",
			"userId": 1
		}
	]`
)

var(
	arrayTest=[]Placeholder{
		{
			ID:     1,
			Title:  "sunt aut facere repellat provident",
			Body:   "quia et suscipitn suscipitn recusandae consequuntur expedita et cumreprehenderit",
			UserID: 1,
		},
	}
)

func TestMain(m *testing.M) {
	userRepo = NewUserRepo()
	os.Exit(m.Run())
}

func TestGet_MinimalResty(t *testing.T) {
	rst := resty.New()
	httpmock.ActivateNonDefault(rst.GetClient())
	defer httpmock.DeactivateAndReset()

	s := &apiDao{
		client: rst,
	}
	httpmock.RegisterResponder("GET", url,
		httpmock.NewStringResponder(200, jsonBody))

	got, err := s.Get()

	assert.Nil(t, err)
	assert.NotNil(t, got)
	assert.EqualValues(t, 1,len(*got))
}

func TestGet(t *testing.T) {
	tests := map[string]struct {
		data     string
		path     string
		respCode int
		want     *[]Placeholder
		wantErr  error
	}{
		"happy path": {
			data:     jsonBody,
			path:     "https://jsonplaceholder.typicode.com/posts",
			respCode: 200,
			want:     &arrayTest,
			wantErr:  nil,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {

			defer httpmock.DeactivateAndReset()

			rst := resty.New()

			s := &apiDao{
				client: rst,
			}

			httpmock.ActivateNonDefault(rst.GetClient())
			httpmock.RegisterResponder("GET", tt.path, newResponder(tt.respCode, tt.data, "application/json"))

			got, err := s.Get()

			if tt.wantErr != nil && !errors.Is(*err, tt.wantErr) {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}


			assert.Equal(t, tt.want, got)
		})
	}
}

func newResponder(s int, c string, ct string) httpmock.Responder {
	resp := httpmock.NewStringResponse(s, c)
	resp.Header.Set("Content-Type", ct)

	return httpmock.ResponderFromResponse(resp)
}
