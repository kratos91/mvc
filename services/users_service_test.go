package services

import (
	"errors"
	"testing"

	"bitbucket.org/kratos91/mvc/domain"
	"github.com/stretchr/testify/assert"
)

var (
	userDaoMock usersDaoMock

	getUserFunction func(userId int64) (*domain.User, *error)
)

type usersDaoMock struct{}

func (m *usersDaoMock) GetUser(userId int64) (*domain.User, *error) {
	return getUserFunction(userId)
}

func TestGetUser_NoFoundInDatabase(t *testing.T) {
	getUserFunction = func(userId int64) (*domain.User, *error) {
		err := errors.New("user not found")
		return nil, &err
	}

	userService := NewUserService(&userDaoMock)
	user, err := userService.GetUser(0)

	assert.Nil(t, user)
	assert.NotNil(t, err)
}
