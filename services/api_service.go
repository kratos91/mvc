package services

import (
	"bitbucket.org/kratos91/mvc/domain"
)

type apiService struct {
	repo domain.ApiRepo
}

type ApiServiceImpl interface {
	Get() (*[]domain.Placeholder, *error)
	Post(*domain.Placeholder) (*domain.Placeholder, *error)
}

func (a *apiService) Get() (*[]domain.Placeholder, *error) {
	tests, err := a.repo.Get()
	if err != nil {
		return nil, err
	}
	return tests, nil
}

func (a *apiService) Post(test *domain.Placeholder) (*domain.Placeholder, *error) {
	resp, err := a.repo.Post(test)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func NewApiService(repo domain.ApiRepo) ApiServiceImpl {
	return &apiService{repo: repo}
}
