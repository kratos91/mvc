package services

import "bitbucket.org/kratos91/mvc/domain"

type userService struct {
	repo domain.UserRepo
}

type UserServiceImpl interface {
	GetUser(int64) (*domain.User, *error)
}

func (u *userService) GetUser(userId int64) (*domain.User, *error) {
	return u.repo.GetUser(userId)
}

func NewUserService(repo domain.UserRepo) UserServiceImpl {
	return &userService{repo: repo}
}
